from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import mysql.connector
import database
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# Configurer CORS (Cross-Origin Resource Sharing)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Vous pouvez spécifier ici les origines autorisées
    allow_credentials=True,
    allow_methods=["*"],  # Méthodes HTTP autorisées
    allow_headers=["*"],  # Vous pouvez spécifier ici les en-têtes autorisés
)

# Opérations CRUD pour la table Etablissement
class Etablissement(BaseModel):
    nom: str
    montant_budget: float
    
@app.post("/etablissement/")
async def create_etablissements(etablissement: Etablissement):
    try:
        cursor = database.connection.cursor() 
        query = "INSERT INTO Etablissement ( nom, montant_budget) VALUES (%s, %s)"
        values = (etablissement.nom, etablissement.montant_budget)
        cursor.execute(query, values)
        database.connection.commit()
        return{"message": "Etablissement créé avec succès"}
    except mysql.connector.Error as err:
     print("Erreur MySQL:", err)
    raise HTTPException(status_code=500, detail="Erreur interne")

    
@app.get("/etablissement")
async def liste_etablissements():
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT * FROM Etablissement"
    cursor.execute(query)
    etablissements = cursor.fetchall()
    if etablissements is None:
        raise HTTPException(status_code=404, detail= "Etablissement not found")
    return etablissements

@app.get("/etablissement_id")
async def liste_etablissements():
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT num_etab, nom FROM Etablissement"
    cursor.execute(query)
    num_etablissements = cursor.fetchall()
    if num_etablissements is None:
        raise HTTPException(status_code=404, detail= "num_Etablissement not found")
    return num_etablissements

@app.get("/etablissement/{num_etab}")
async def read_id_etablissement(num_etab: int):
    # Récupérer l'élément de la base de données
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT * FROM Etablissement WHERE num_etab = %s"
    cursor.execute(query, (num_etab,))
    etablissements = cursor.fetchone()
    if etablissements is None:
        raise HTTPException(status_code=404, detail="Etablissement not found")
    return etablissements


@app.put("/etablissement/{num_etab}")
async def update_etablissement(num_etab: int, etablissement: Etablissement):
    try:
        cursor = database.connection.cursor()
        query = "UPDATE Etablissement SET nom=%s, montant_budget=%s WHERE num_etab=%s"
        values = (etablissement.nom, etablissement.montant_budget, num_etab)
        cursor.execute(query,values)
        database.connection.commit()
        return {"message": "Etablissement update"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Update error")
    
@app.delete("/etablissement/{num_etab}")
async def delete_etablissement(num_etab: int):
    try:
        cursor = database.connection.cursor()
        query = "DELETE FROM Etablissement WHERE num_etab = %s"
        cursor.execute(query, (num_etab,))
        database.connection.commit()
        return {"message": "Etablissement deleted"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Delete error")
    
# Opérations CRUD pour la table Depense

class Depense(BaseModel):
    num_etab: int
    depense: float
    
@app.post("/depense/")
async def create_depense(depense: Depense):
    try:
        cursor = database.connection.cursor() 
        query = "INSERT INTO Depense ( num_etab, depense) VALUES (%s, %s)"
        values = (depense.num_etab, depense.depense)
        cursor.execute(query, values)
        database.connection.commit()
        return{"message": "Depense créé avec succès"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Erreur interne")
    
@app.get("/depense/")
async def liste_depense():
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT * FROM Depense"
    cursor.execute(query)
    depense = cursor.fetchall()
    if depense is None:
        raise HTTPException(status_code=404, detail= "Depense not found")
    return depense

@app.get("/depense/{num_depense}")
async def read_id_depense(num_depense: int):
    # Récupérer l'élément de la base de données
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT * FROM Depense WHERE num_depense = %s"
    cursor.execute(query, (num_depense,))
    depense = cursor.fetchone()
    if depense is None:
        raise HTTPException(status_code=404, detail="Depense not found")
    return depense

@app.put("/depense/{id_depense}")
async def update_depense(id_depense: int, depense: Depense):
    try:
        cursor = database.connection.cursor()
        query = "UPDATE Depense SET num_etab=%s, depense=%s WHERE num_depense=%s"
        values = (depense.num_etab, depense.depense, id_depense)
        cursor.execute(query,values)
        database.connection.commit()
        return {"message": "Depense update"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Update error")

@app.delete("/depense/{num_depense}")
async def delete_depense(num_depense: int):
    try:
        cursor = database.connection.cursor()
        query = "DELETE FROM Depense WHERE num_depense = %s"
        cursor.execute(query, (num_depense,))
        database.connection.commit()
        return {"message": "Depense deleted"}
    except mysql.connector.Error as err:
        raise HTTPException(status_code=500, detail="Delete error")
    
# Définition du modèle pour les opérations sur la table Audit_depense
class AuditDepenseModel(BaseModel):
    type_action: str
    num_depense: int
    nom: str
    ancien_depense: float
    nouveau_depense: float
    utilisateur: str
    
# Point de terminaison pour récupérer la liste des audits de dépenses
@app.get("/audit_depense")
async def liste_audit_depense():
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT * FROM Audit_Depense"
    cursor.execute(query)
    audit_depense = cursor.fetchall()
    if audit_depense is None:
        raise HTTPException(status_code=404, detail= "Audit_Depense not found")
    return audit_depense

    
@app.get("/total_type_action")
async def liste_total_type_action():
    cursor = database.connection.cursor(dictionary=True)
    query = "SELECT type_action, COUNT(*) AS total FROM Audit_Depense GROUP BY type_action;"
    cursor.execute(query)
    total_action = cursor.fetchall()
    if total_action is None:
        raise HTTPException(status_code=404, detail= "Total_Action not found")
    return total_action