import mysql.connector

db_config = {
    "host": "mysql-service.default",
    "user": "root",
    "password": "admin",
    "database": "gestion_depenses"
}

try:
    connection = mysql.connector.connect(**db_config)
    print("Connecté à la base de données MySQL")
except mysql.connector.Error as err:
    print(f"Erreur : {err}")
