import React from "react";
import { BrowserRouter as Router, Routes, Route, Navigate } from "react-router-dom";
import { NavBar } from "./component/NavBarComponent/NavBar";
import { Audit } from './component/AuditComponent/audit';
import { Etablissement } from './component/EtablissementComponent/etablissement';
import { Depense } from './component/DepenseComponent/depense';
import { Login } from './component/Auth/login';

function AuthenticatedApp() {
  return (
    <div>
      <NavBar />
      <Routes>
        <Route path="/audit" element={<Audit />} />
        <Route path="/etablissement" element={<Etablissement />} />
        <Route path="/depense" element={<Depense />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </div>
  );
}

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/*" element={<AuthenticatedApp />} />
      </Routes>
    </Router>
  );
}

export default App;
