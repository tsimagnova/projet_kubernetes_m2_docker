import { useState } from 'react';
import { styled } from '@mui/material/styles';
import { useForm, Controller } from "react-hook-form"; // Ajoutez cette ligne
import PropTypes from 'prop-types';
import {
    Button,
    IconButton,
    TextField,
    Container,
    Box,
    MenuItem,
    Snackbar
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import CloseIcon from '@mui/icons-material/Close';
import AddIcon from '@mui/icons-material/Add';
import SaveIcon from '@mui/icons-material/Save';
import Alert from '@mui/material/Alert';
import axiosInstance from "../../service/axios";
import Swal from "sweetalert2";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

function BootstrapDialogTitle(props) {
    const { children, onClose, ...other } = props;

    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={onClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}

BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
};

export default function AddComponentEtablissement(props) {
    const [open, setOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const {
        control,
        register,
        handleSubmit,
        setValue,
        formState: { errors, isSubmitting },
    } = useForm();


    const onAddSubmit = async (values) => {
        console.log(values);
        try {
            const response = await axiosInstance.post("/etablissement/", values);
            setLoading(false);
            handleClose();
            props.fetchStock();
            //const newetudiant = response.data;
            // setEtudiants((prevetudiants) => [...prevetudiants, newetudiant]); 
            setSnackbarOpen(true);
        } catch (error) {
            handleClose();
            Swal.fire("Erreur", error.toString(), "error");
            setLoading(false);
        } 
    };

    return (
        <>
            <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={() => setSnackbarOpen(false)}>
                <Alert onClose={() => setSnackbarOpen(false)} severity="success" sx={{ width: '100%' }}>
                    Ajout avec succès!
                </Alert>
            </Snackbar>
            <Button variant="contained" color="primary" endIcon={<AddIcon />} onClick={handleClickOpen}>
                NOUVEAU
            </Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    Ajouter le nouveau etablissement
                </BootstrapDialogTitle>
                <form onSubmit={handleSubmit(onAddSubmit)}>
                    <DialogContent dividers>
                        <Container>
                            <Box
                                sx={{
                                    width: 500,
                                    height: 100,
                                    '& .MuiTextField-root': { m: 1, width: '25ch' },
                                }}
                            >
                                <TextField
                                    type="text"
                                    label="Nom"
                                    variant="outlined"
                                    id="nom"
                                    name="nom"
                                    fullWidth
                                    {...register("nom", {
                                        required: " Nom obligatoire",
                                        minLength: {
                                            value: 2,
                                            message: "Nom au moin 2 caractères",
                                        },
                                        maxLength: {
                                            value: 50,
                                            message: "Nom au maximum 50 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.nom)}
                                    helperText={errors.nom?.message}
                                />

                                <TextField
                                    type="text"
                                    label="Budget"
                                    variant="outlined"
                                    id="budget"
                                    name="budget"
                                    fullWidth
                                    {...register("montant_budget", {
                                        required: "Budget obligatoire",
                                        pattern: {
                                            value: /^(0|[1-9]\d*)(\.\d+)?$/,
                                            message: "Le budget doit être un nombre décimal supérieur ou égal à zéro.",
                                        },
                                        minLength: {
                                            value: 1,
                                            message: "Le budget doit avoir au moins 1 caractère",
                                        },
                                        maxLength: {
                                            value: 50,
                                            message: "Le budget doit avoir au maximum 50 caractères",
                                        },
                                    })}
                                    error={Boolean(errors.montant_budget)}
                                    helperText={errors.montant_budget?.message}
                                />
                            </Box>
                        </Container>
                    </DialogContent>
                    <DialogActions>
                        {loading ? (
                            <Button>
                                Loading...
                            </Button>
                        ) : (<Button variant='outlined' endIcon={<SaveIcon />} type="submit">
                            Enregistrer
                        </Button>)}

                    </DialogActions>
                </form>
            </BootstrapDialog>
        </>
    );
}
