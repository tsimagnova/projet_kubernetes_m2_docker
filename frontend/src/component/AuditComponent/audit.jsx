import React, { useState, useRef, useEffect } from "react";
import axiosInstance from "../../service/axios";
import {
    Box,
    Snackbar,
    Typography,
    Stack,
    Chip,
    Pagination,
    TextField,
    CircularProgress,
    Paper,
    IconButton,
    TableRow,
    TableHead,
    TableContainer,
    TableCell,
    TableBody,
    Table,
} from '@mui/material';
import DeleteIcon from "@mui/icons-material/Delete";
import MuiAlert from '@mui/material/Alert';
import Swal from "sweetalert2";
//import EditComponentMatiere from "./editMatiere";
//import AddComponentDepense from "./addDepense";

export const Audit = () => {
    const [stock, setStock] = useState([]);
    const [action, setAction] = useState([]);
    const [loading, setLoading] = useState(false);
    const [deletingIds, setDeletingIds] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [currentPage, setCurrentPage] = useState(1);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [totalPages, setTotalPages] = useState(1);
    const [open, setOpen] = useState(false);
    const [totalItems, setTotalItems] = useState(0);
    const isMounted = useRef(false);

    const fetchStock = async (search = "") => {
        setLoading(true);
        try {
            const res = await axiosInstance.get("/audit_depense");
            const resAction = await axiosInstance.get("/total_type_action");
            console.log(resAction.data);
            console.log(res.data);
            const searchData = search
                ? res.data.filter((item) =>
                    Object.values(item)
                        .join(" ")
                        .toLowerCase()
                        .includes(search.toLowerCase())
                )
                : res.data;

            setTotalItems(searchData.length);

            const newTotalPages = Math.ceil(searchData.length / rowsPerPage);
            const newCurrentPage = Math.min(currentPage, newTotalPages);
            const startIndex = (newCurrentPage - 1) * rowsPerPage;
            const endIndex = startIndex + rowsPerPage;
            const paginatedData = searchData.slice(startIndex, endIndex);

            setStock(paginatedData);
            setCurrentPage(newCurrentPage);
            setTotalPages(newTotalPages);
            setAction(resAction.data);
        } catch (error) {
            console.error(error);
            Swal.fire("Erreur", error.toString(), "error");
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (isMounted.current) {
            fetchStock(searchText);
        } else {
            isMounted.current = true;
            fetchStock(searchText);
        }
    }, [searchText, currentPage, rowsPerPage]);

    const handleChangePage = (event, newPage) => {
        setCurrentPage(newPage);
    };

    const handleSearchChange = (event) => {
        const newSearchText = event.target.value;
        // console.log("Search text changed:", newSearchText);
        setSearchText(newSearchText);

        // Reset data when search text is cleared
        if (newSearchText === "") {
            fetchStock(""); // Pass an empty string for search
        } else {
            fetchStock(newSearchText);
        }
    };


    const handleClick = (event, Niveau) => {
        event.preventDefault();
        setCurrentPage(1); // Reset page when a filter is applied
        setSearchText("");
    };

    const deleteStock = (id) => {
        setDeletingIds((prevIds) => [...prevIds, id]);

        axiosInstance
            .delete(`/todo/${id}`)
            .then(() => {
                setOpen(true);
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
                setStock((prevstock) => prevstock.filter((stock) => stock.todo_id !== id));
            })
            .catch((error) => {
                Swal.fire("Erreur", error.toString(), "error");
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
            });
    };

    const Supprimer = (id) => {
        Swal.fire({
            title: "Etes-vous sûre?",
            text: "Cette action est irreversible!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            cancelButtonText: "Annuler",
            confirmButtonText: "Supprimer",
        }).then((result) => {
            if (result.isConfirmed) {
                deleteStock(id);
            }
        });
    };

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

   /* const fetchAction = async () => {
        try {
            const resAction = await axiosInstance.get("/total_type_action");
            console.log(resAction.data);
            setAction(resAction.data);
        } catch (error) {
            console.error(error);
            Swal.fire("Erreur", error.toString(), "error");
        }
    }; */
    
    return (
        <Box sx={{ p: 3, mt: 10 }}>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Suppression avec succès
                </Alert>
            </Snackbar>
            <Box sx={{ display: "flex", justifyContent: "flex-end", mb: 2 }}>
                <TextField id="outlined-search" label="Rechercher" type="search" size="small" value={searchText} onChange={handleSearchChange} />
            </Box>
            <Typography align="center" variant="h5" sx={{ mt: 2 }}> Liste des actions faite par l'utilisateur </Typography>
            <TableContainer component={Paper} sx={{ mt: 2 }}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center"> ID</TableCell>
                            <TableCell align="center"> type d'action</TableCell>
                            <TableCell align="center"> date d'operation</TableCell>
                            <TableCell align="center"> Identifient de depense</TableCell>
                            <TableCell align="center"> Nom de l'établissement</TableCell>
                            <TableCell align="center"> Ancienne dépense</TableCell>
                            <TableCell align="center"> Nouvelle dépense</TableCell>
                            <TableCell align="center"> Utilisateur</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <TableRow>
                                <TableCell colSpan={7} align="center">
                                    Loading...
                                </TableCell>
                            </TableRow>
                        ) : stock.length === 0 ? (
                            <TableRow>
                                <TableCell colSpan={9} align="center">
                                    Aucune donnée pour l'instant.
                                </TableCell>
                            </TableRow>
                        ) : (
                            stock.map((item) => (
                                <TableRow
                                    key={item.id}
                                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row" align="center">
                                        {item.id}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        <Chip
                                            color={
                                                item.type_action === "ajout" ? "info" :
                                                    item.type_action === "modification" ? "warning" :
                                                        item.type_action === "suppression" ? "error" :
                                                            "default"
                                            }
                                            label={item.type_action}
                                        />
                                    </TableCell>

                                    <TableCell component="th" scope="row" align="center">
                                        Date {new Date(item.date_operation).toLocaleDateString()} Heure : {new Date(item.date_operation).toLocaleTimeString()}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.num_depense}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.nom}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.ancien_depense}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.nouveau_depense}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.utilisateur}
                                    </TableCell>
                                    <TableCell align="center">
                                        {/* <EditComponentMatiere
                                            id={matiere.id}
                                            ue={matiere.ue}
                                            nom={matiere.nomMatiere}
                                            niveau={matiere.niveau}
                                            parcours={matiere.parcours}
                                            semestre={matiere.semestre}
                                            coefficient={matiere.coefficient}
                                            poids={matiere.poids}
                                            credits={matiere.creditsEC}
                                            idEnseignant={matiere.id_enseignant}
                                            matieres={matieres}
                                            setMatieres={setMatieres}
                                        /> */}
                                        <IconButton
                                            aria-label="delete"
                                            size="small"
                                            color="error"
                                            onClick={() => Supprimer(item.id)}
                                        >
                                            {deletingIds.includes(item.id) ? (
                                                <CircularProgress color="secondary" size={20} />
                                            ) : (
                                                <DeleteIcon fontSize="small" />
                                            )}
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            <div style={{ display: "flex", justifyContent: "flex-end", marginTop: "20px" }}>
                <Stack spacing={2}>
                    <Pagination
                        color="primary"
                        count={totalPages}
                        page={currentPage}
                        onChange={handleChangePage}
                    />
                </Stack>
            </div>
            {/*action.length > 0 && action.map((item) => (
                <Typography align="right" variant="h6" sx={{ mt: 2 }}> Total {item.suppression}  </Typography>
            ))*/}
           {loading? (
            <Typography align="right" variant="h6" sx={{ mt: 2 }}> Loading...  </Typography>
           ) : stock.length === 0 ?(
            <Typography align="right" variant="h6" sx={{ mt: 2 }}> Aucune information  </Typography>
           ) :(
            action.map((item, index) => (
                <Typography key={index} align="center" variant="h6" sx={{ mt: 2 }}> Total  {item.type_action} : {item.total}  </Typography>
            ))
           )}
        </Box>
    );
};
