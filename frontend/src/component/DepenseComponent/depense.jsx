import React, { useState, useRef, useEffect } from "react";
import axiosInstance from "../../service/axios";
import {
    Box,
    Snackbar,
    Typography,
    Stack,
    Chip,
    Pagination,
    TextField,
    CircularProgress,
    Paper,
    IconButton,
    TableRow,
    TableHead,
    TableContainer,
    TableCell,
    TableBody,
    Table,
} from '@mui/material';
import DeleteIcon from "@mui/icons-material/Delete";
import MuiAlert from '@mui/material/Alert';
import Swal from "sweetalert2";
import EditComponentDepense from "./editDepense";
import AddComponentDepense from "./addDepense";

export const Depense = () => {
    const [stock, setStock] = useState([]);
    const [loading, setLoading] = useState(false);
    const [deletingIds, setDeletingIds] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [currentPage, setCurrentPage] = useState(1);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [totalPages, setTotalPages] = useState(1);
    const [open, setOpen] = useState(false);
    const [totalItems, setTotalItems] = useState(0);
    const isMounted = useRef(false);



    const fetchStock = async (search = "") => {
        setLoading(true);
        try {
            const res = await axiosInstance.get("/depense");
            console.log(res.data);
            const searchData = search
                ? res.data.filter((item) =>
                    Object.values(item)
                        .join(" ")
                        .toLowerCase()
                        .includes(search.toLowerCase())
                )
                : res.data;

            setTotalItems(searchData.length);

            const newTotalPages = Math.ceil(searchData.length / rowsPerPage);
            const newCurrentPage = Math.min(currentPage, newTotalPages);
            const startIndex = (newCurrentPage - 1) * rowsPerPage;
            const endIndex = startIndex + rowsPerPage;
            const paginatedData = searchData.slice(startIndex, endIndex);

            setStock(paginatedData);
            setCurrentPage(newCurrentPage);
            setTotalPages(newTotalPages);
        } catch (error) {
            console.error(error);
            Swal.fire("Erreur", error.toString(), "error");
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        if (isMounted.current) {
            fetchStock(searchText);
        } else {
            isMounted.current = true;
            fetchStock(searchText);
        }
    }, [searchText, currentPage, rowsPerPage]);

    const handleChangePage = (event, newPage) => {
        setCurrentPage(newPage);
    };

    const handleSearchChange = (event) => {
        const newSearchText = event.target.value;
        // console.log("Search text changed:", newSearchText);
        setSearchText(newSearchText);

        // Reset data when search text is cleared
        if (newSearchText === "") {
            fetchStock(""); // Pass an empty string for search
        } else {
            fetchStock(newSearchText);
        }
    };


    const handleClick = (event, Niveau) => {
        event.preventDefault();
        setCurrentPage(1); // Reset page when a filter is applied
        setSearchText("");
    };

    const deleteStock = (id) => {
        setDeletingIds((prevIds) => [...prevIds, id]);

        axiosInstance
            .delete(`/depense/${id}`)
            .then(() => {
                setOpen(true);
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
                setStock((prevstock) => prevstock.filter((stock) => stock.num_depense !== id));
            })
            .catch((error) => {
                Swal.fire("Erreur", error.toString(), "error");
                setDeletingIds((prevIds) => prevIds.filter((prevId) => prevId !== id));
            });
    };

    const Supprimer = (id) => {
        Swal.fire({
            title: "Etes-vous sûre?",
            text: "Cette action est irreversible!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            cancelButtonText: "Annuler",
            confirmButtonText: "Supprimer",
        }).then((result) => {
            if (result.isConfirmed) {
                deleteStock(id);
            }
        });
    };

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <Box sx={{ p: 3, mt: 10 }}>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Suppression avec succès
                </Alert>
            </Snackbar>
            <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: "center", mb: 2 }}>
                <AddComponentDepense fetchStock={fetchStock}  />
                <TextField id="outlined-search" label="Rechercher" type="search" size="small" value={searchText} onChange={handleSearchChange} />
            </Box>
            <Typography align="center" variant="h5" sx={{ mt: 2 }}> Liste des Depenses </Typography>
            <TableContainer component={Paper} sx={{ mt: 2 }}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="center"> ID</TableCell>
                            <TableCell align="center">Identifient de l'établissement</TableCell>
                            <TableCell align="center">Depense</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <TableRow>
                                <TableCell colSpan={7} align="center">
                                    Loading...
                                </TableCell>
                            </TableRow>
                        ) : stock.length === 0 ? (
                            <TableRow>
                                <TableCell colSpan={9} align="center">
                                    Aucune donnée pour l'instant.
                                </TableCell>
                            </TableRow>
                        ) : (
                            stock.map((item) => (
                                <TableRow
                                    key={item.num_depense}
                                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row" align="center">
                                        {item.num_depense}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">
                                        {item.num_etab}
                                    </TableCell>
                                    <TableCell component="th" scope="row" align="center">{item.depense}</TableCell>
                                    <TableCell align="center">
                                        <EditComponentDepense
                                            id={item.num_depense}
                                            num_etab={item.num_etab}
                                            depense = {item.depense}
                                            fetchStock={fetchStock}
                                        />
                                        <IconButton
                                            aria-label="delete"
                                            size="small"
                                            color="error"
                                            onClick={() => Supprimer(item.num_depense)}
                                        >
                                            {deletingIds.includes(item.num_depense) ? (
                                                <CircularProgress color="secondary" size={20} />
                                            ) : (
                                                <DeleteIcon fontSize="small" />
                                            )}
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            <div style={{ display: "flex", justifyContent: "flex-end", marginTop: "20px" }}>
                <Stack spacing={2}>
                    <Pagination
                        color="primary"
                        count={totalPages}
                        page={currentPage}
                        onChange={handleChangePage}
                    />
                </Stack>
            </div>
        </Box>
    );
};
