import React, { useState, useRef, useEffect } from "react";
import Avatar from '@mui/material/Avatar';
import {
  Button,
  CssBaseline,
  TextField,
  Paper,
  Box,
  Grid,
  Link,
  Typography
} from '@mui/material';
//import { Link as RouterLink } from "react-router-dom";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useForm } from 'react-hook-form';
//import { useNavigate } from 'react-router-dom';
//import { useAuth } from '../../hooks/useAuth';
import image_login from '../../assests/image_login.jpg';

const defaultTheme = createTheme();

export const Login = () => {

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm();

  //const { login } = useAuth();

  const onLoginSubmit = async (values) => {
    console.log(values);
    try {
     // await login(values.email, values.password);
    } catch (error) {
      console.log(error);
    }
  };


  return (
    <ThemeProvider theme={defaultTheme}>
      <Grid container component="main" sx={{ height: '100vh' }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage: `url(${image_login})`,
            backgroundRepeat: 'no-repeat',
            backgroundColor: (t) =>
              t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Authentifiez-vous
            </Typography>
            <form onSubmit={handleSubmit(onLoginSubmit)}>
              <Box>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Adresse e-mail"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  {...register("email", {
                    required: "Email obligatoire",
                    pattern: {
                      value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                      message: "adresse email non valide",
                    },
                  })}
                  error={Boolean(errors.email)}
                  helperText={errors.email?.message}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Mot de passe"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  {...register("password", {
                    required: "Mot de passe obligatoire",
                  })}
                  error={Boolean(errors.password)}
                  helperText={errors.password?.message}
                />
                {isSubmitting ? (
                  <Button
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Chargement...
                  </Button>
                ) : (<Button
                  variant='contained'
                  type="submit"
                  fullWidth
                  sx={{ mt: 3, mb: 2 }}
                >
                  Se connecter
                </Button>)}
                <Typography variant="body2" align="center" sx={{ mt: 3 }}>
                  <Link href="/register">
                    {"Avez-vous un compte? S'inscrire"}
                  </Link>
                </Typography>
              </Box>
            </form>

          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>

  );
};
